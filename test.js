let itemArray = [];
load()
class Todo{
 constructor(id,value,status,date) {
    this.id = id;
    this.task = value;
    this.status =status ;
    this.creation_date =date;
}
}
 
function load() {
    let data = JSON.parse(localStorage.getItem("dataItems"));
    if (data) {
        itemArray = data;
        $("ul").empty();
        display();
    } else {
        $("ul").empty();
    }
}

function display() {
    if (itemArray != undefined || itemArray !== null) {
        itemArray.forEach(element => {
            //  adder(element)
            check = $('<input/>').attr({
                type: 'checkbox',
                class: 'check-box',
                id: element.id
            })
            let button = $('<input/>').attr({
                type: 'button',
                class: 'del_item',
                id: element.id
            })
            // let edit = $('<input/>').attr({
            //     type: 'button',
            //     class: 'edit_item',
            //     id: element.id
            // })
            let content = $('<div>').attr({
                class: 'content-dev',
                id: element.id
            }).css({textDecoration: getMark(element.status)},).html(element.task)
            let li = $("<li/>").attr({
                class: 'list_item',
                id: element.id,
                draggable:true
            })
            if (element["status"]) {
                $(check).attr("checked", "true");
            }
            let output = (li).append(check).append(content).append(button);//.append(edit);
            $("ul").prepend(output)

            let listItem = $('.list_item');
            [].forEach.call(listItem, function(item){
                  addEventsDragAndDrop(item);
            });
        });
    }
}

function getMark(value) {
    return (value === 1) ? "line-through" : "none";
}

function addEventsDragAndDrop(el) {
    el.addEventListener('dragstart', dragStart, false);
    el.addEventListener('dragover', dragOver, false);
    el.addEventListener('drop', dragDrop, false);
}

function dragStart(e) {
    currEl = this;
    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData('text/html', this.innerHTML);
}

function dragOver(e) {
    e.preventDefault();
    e.dataTransfer.dropEffect = 'move';
}

function dragDrop(e) {
    if (currEl != this) {
        currEl.innerHTML = this.innerHTML;
        this.innerHTML = e.dataTransfer.getData('text/html');
        let id_target = this.id;
        let id_current = currEl.id;
        let target = 0;
        let current = 0;
        itemArray.forEach(function(val,i)
        {
            if(val.id == id_target)
             target=i;
            if(val.id == id_current)
             current=i;
        });
        let temp = itemArray[target];
        itemArray[target] = itemArray[current];
        itemArray[current] = temp;
        localStorage.setItem('dataItems', JSON.stringify(itemArray));
        itemArray = []
        load();
    }

}
$('.text-box').keyup(function(e){
    if(e.keyCode == 13)
    {
       createList();
    }
});

function createList() {
    //let d = document.getElementById("text");
    let d=$("#text")[0];
    ids = Date.now()
    date=new Date()
    if (d.value != "") 
    {
        var a=new Todo(ids,d.value,0,date);
        itemArray.push(a)
        $("#text")[0].value = "";
        localStorage.setItem("dataItems", JSON.stringify(itemArray));
        itemArray = [];
        load()
    } 


}
$("ul").on("change", "li .check-box", (changeStatus));

function changeStatus() {
    let id = $(this).next()[0].id;
    itemArray.forEach(record => {
        if (record.id == id) {

            if ($(this)[0].checked == true) {
                record.status = 1;
                let text = $(this).next().css({"text-decoration": "line-through"});
            } else {
                record.status = 0;
                let text = $(this).next().css({"text-decoration": "none"});
            }
        }
        localStorage.setItem("dataItems", JSON.stringify(itemArray));
    });

}
$("ul").on("click", "li .del_item", removeItem);

function removeItem() {

    let id = $(this)[0].id;
    itemArray = itemArray.filter(val => 
    {
        return ( val.id!=id || val.status==false)
    })
   
    localStorage.setItem("dataItems", JSON.stringify(itemArray));
    load()

}

$("ul").on("dblclick", "li .edit_item", (edit_data));

function edit_data() {
    edited = $(this).siblings()[1]
    $(edited).attr("contentEditable", "true");

}
$("ul").on("dblclick","li .content-dev",
    function (e) {
        $($(this)[0]).attr("contentEditable", "true");
    }
).on('blur',"li .content-dev" ,function(){
    $($(this)[0]).attr("contentEditable", "false");
    let text = $(this)[0].innerText;
        id = $(this)[0].id;
        itemArray.forEach(e => {
            if ((id == e.id)) {
                e.task = text;
            }
        });
        localStorage.setItem("dataItems", JSON.stringify(itemArray));
});
$("ul").on("keypress", '.content-dev', function (event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        var text = $(this)[0].innerText;
        id = $(this)[0].id;
        itemArray.forEach(e => {
            if ((id == e.id)) {
                e.task= text;
            }
        });
        localStorage.setItem("dataItems", JSON.stringify(itemArray));
    }
});


function deleteList() {
    localStorage.clear();
    load();
    itemArray = [];
}
